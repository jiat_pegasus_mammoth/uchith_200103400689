import com.uchith.web.db.MySQL;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;

@WebServlet(name = "DeleteUser", value = "/deleteUser")
public class DeleteUser extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id=request.getParameter("id");
        Connection connection=null;
        try{
            connection=MySQL.getConnection();
            connection.createStatement().executeUpdate("DELETE FROM `user` WHERE `id`='"+id+"';");
            response.getWriter().write("Deleted");
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try{
                    connection.close();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

}
