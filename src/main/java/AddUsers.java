import com.uchith.web.db.MySQL;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "AddUsers", value = "/addUsers")
public class AddUsers extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String fName=request.getParameter("fname");
        String lName=request.getParameter("lname");
        String email=request.getParameter("email");
        Connection connection=null;
        try{
            connection=MySQL.getConnection();
            connection.createStatement().executeUpdate("INSERT INTO `user`(`first_name`,`last_name`,`email`) VALUES ('"+fName+"','"+lName+"','"+email+"');");
            response.getWriter().write("Success");
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
