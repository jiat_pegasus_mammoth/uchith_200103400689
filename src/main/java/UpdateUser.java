import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uchith.web.db.MySQL;


import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;

@WebServlet(name = "UpdateUser", value = "/updateUser")
public class UpdateUser extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id=request.getParameter("id");
        Connection connection=null;
        if(request.getParameter("type").equals("search")){
            try{
                connection=MySQL.getConnection();
                ResultSet resultSet= connection.createStatement().executeQuery("SELECT * FROM `user` WHERE `user`.`id`='"+id+"';");
                if(resultSet.next()){
                    String arr="{'id':'"+resultSet.getString("id")+"','first_name':'"+resultSet.getString("first_name")+"','last_name':'"+resultSet.getString("last_name")+"','email':'"+resultSet.getString("email")+"'}";
                    JsonObject obj= (JsonObject) JsonParser.parseString(arr);
                    response.getWriter().write(obj.toString());
                }else{
                    response.getWriter().write("Error2");
                }

            }catch(Exception e){
                e.printStackTrace();
            }finally {
                if(connection!=null){
                try{
                    connection.close();
                }catch(Exception e){
                    e.printStackTrace();
                }

                }
            }
        } else if (request.getParameter("type").equals("update")) {
            try{
                connection=MySQL.getConnection();
                connection.createStatement().executeUpdate("UPDATE `user` SET `first_name`='"+request.getParameter("fname")+"',`last_name`='"+request.getParameter("lname")+"',`email`='"+request.getParameter("email")+"' WHERE `id`='"+id+"';");
                response.getWriter().write("Success");
            }catch(Exception e){
                e.printStackTrace();
            }finally {
                if(connection!=null){
                    try{
                        connection.close();
                    }catch(Exception e){
                        e.printStackTrace();
                    }

                }
            }
        }else{
            response.getWriter().write("Error1");
        }

    }

}
