function addUser() {
    let fName = document.getElementById("fname");
    let lName = document.getElementById("lname");
    let email = document.getElementById("email");
    let r = new XMLHttpRequest();
    r.onreadystatechange = function () {
        if (r.readyState == 4) {
            let text = r.responseText;
            alert(text);
        }
    };
    r.open("GET", "addUsers?fname=" + fName.value + "&lname=" + lName.value + "&email=" + email.value, true);
    r.send();
}

function updateUser(id) {
    let r = new XMLHttpRequest();
    r.onreadystatechange = function () {
        if (r.readyState == 4) {
            let text = r.responseText;
            if (text == "Error") {
                alert(text);
            } else {
                let obj = JSON.parse(text);
                document.getElementById("fname").value = obj.first_name;
                document.getElementById("lname").value = obj.last_name;
                document.getElementById("email").value = obj.email;
                let btn = document.createElement("button");
                btn.appendChild(document.createTextNode("Update"));
                btn.setAttribute("onclick", "doUpdate(" + id + ")");
                btn.setAttribute("id", "btn");
                let td=document.getElementById("update-btn");
                td.innerHTML='';
                td.appendChild(btn);
            }
        }
    };
    r.open("GET", "updateUser?id=" + id + "&type=search", true);
    r.send();
}

function doUpdate(id) {
    let fName = document.getElementById("fname");
    let lName = document.getElementById("lname");
    let email = document.getElementById("email");
    let r = new XMLHttpRequest();
    r.onreadystatechange = function () {
        if (r.readyState == 4) {
            let text = r.responseText;
            alert(text);
        }
    };
    r.open("GET", "updateUser?fname=" + fName.value + "&lname=" + lName.value + "&email=" + email.value + "&id=" + id + "&type=update", true);
    r.send();
}
function deleteUser(id){
    let r=new XMLHttpRequest();
    r.onreadystatechange=function(){
        if(r.readyState==4){
            let text=r.responseText;
            alert(text);
        }
    };
    r.open("GET","deleteUser?id="+id,true);
    r.send();
}