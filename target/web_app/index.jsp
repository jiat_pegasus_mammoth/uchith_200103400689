<%@ page import="com.uchith.web.db.MySQL" %>
<%@ page import="java.sql.ResultSet" %><%--
  Created by IntelliJ IDEA.
  User: uchithvihanga
  Date: 2023-03-31
  Time: 15:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <table>
        <tr>
            <td>First Name</td>
            <td><input type="text" id="fname"/></td>
        </tr>
        <tr>
            <td>Last Name</td>
            <td><input type="text" id="lname"/></td>
        </tr>
        <tr>
            <td>Email</td>
            <td><input type="email" id="email"/></td>
        </tr>
        <tr>
            <td><button onclick="addUser();">Submit</button></td>
            <td id="update-btn"></td>
        </tr>
    </table>
    <br/>
    <table>
        <tr>
            <th>ID</th>
            <th>FIRST NAME</th>
            <th>LAST NAME</th>
            <th>EMAIL</th>
            <th></th>
            <th></th>
        </tr>
        <%
            ResultSet resultSet=MySQL.getConnection().createStatement().executeQuery("SELECT * FROM `user`;");
            while (resultSet.next()){%>
        <tr>
            <td><%=resultSet.getString("id")%></td>
            <td><%=resultSet.getString("first_name")%></td>
            <td><%=resultSet.getString("last_name")%></td>
            <td><%=resultSet.getString("email")%></td>
            <td><button onclick="updateUser(<%=resultSet.getString("id")%>);">Update</button></td>
            <td><button onclick="deleteUser(<%=resultSet.getString("id")%>);">Delete</button></td>

        </tr>
        <%}%>
    </table>
<script src="js/script.js"></script>
</body>
</html>
